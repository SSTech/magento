FROM php:7.1-fpm-alpine as magento2_phpfpm_build

ENV WORKDIR=/magento

WORKDIR $WORKDIR

RUN adduser -S application -G www-data && chown -R application:www-data $WORKDIR

RUN apk add --no-cache unzip git redis curl gcc make autoconf $PHPIZE_DEPS libxml2-dev libpng-dev icu-dev \
      libpng-dev icu-dev libmcrypt-dev libxslt-dev

RUN yes '' | pecl install igbinary lzf && yes | pecl install redis
RUN docker-php-ext-install soap bcmath gd intl mcrypt xsl pdo_mysql zip

RUN \
      echo "memory_limit=768M" >> /usr/local/etc/php/conf.d/memory.ini && \
      echo 'extension=lzf.so' >> /usr/local/etc/php/conf.d/lzf.ini && \
      echo 'extension=redis.so' >> /usr/local/etc/php/conf.d/redis.ini && \
      echo 'extension=igbinary.so' >> /usr/local/etc/php/conf.d/igbinary.ini && \
      curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer

USER application

ADD auth.json /home/application/auth.json

ENV COMPOSER_HOME=/home/application

RUN composer global require hirak/prestissimo

ADD --chown=application:www-data composer.json $WORKDIR/composer.json
ADD --chown=application:www-data composer.lock $WORKDIR/composer.lock

RUN composer install --no-ansi --no-interaction --optimize-autoloader --no-dev --prefer-dist

ADD --chown=application:www-data ./docker/phpfpm/env.php /env.php
ADD --chown=application:www-data ./docker/phpfpm/env.php $WORKDIR/app/etc/env.php

ADD ./docker/phpfpm/etc /usr/local/etc/php
ADD ./docker/phpfpm/etc/environment /etc/

USER root

ADD ./docker/phpfpm/run.sh /run.sh

RUN ls -alh /usr/local/include/php/ext

FROM php:7.1-fpm-alpine as magento2

ENV WORKDIR=/magento

WORKDIR $WORKDIR

RUN adduser -S application -G www-data && chown -R application:www-data $WORKDIR

RUN apk add --no-cache gettext varnish apache2 apache2-utils apache2-proxy

COPY --chown=application:www-data --from=magento2_phpfpm_build $WORKDIR $WORKDIR
COPY --from=magento2_phpfpm_build /usr/local/include/php/ext/* /usr/local/include/php/ext/
COPY --from=magento2_phpfpm_build /usr/local/etc/php/conf.d/* /usr/local/etc/php/conf.d/
COPY --from=magento2_phpfpm_build /usr/local/etc/php/php-fpm.d/* /usr/local/etc/php/php-fpm.d/

ADD --chown=application:www-data ./docker/phpfpm/env.php /env.php

ENV PATH_VAR_VARNISH "/var/lib/varnish"
ENV VARNISH_CACHE_COOKIE ""
ENV VARNISH_IGNORE_COOKIE ""
ENV VARNISH_CACHE_AUTH ""
ENV VARNISH_IGNORE_AUTH ""
ENV VARNISH_DEFAULT_TTL ""
ENV VARNISH_BACKEND_IP ""
ENV VARNISH_BACKEND_PORT ""
ENV VARNISH_MEM "1G"
ENV FILE_DEFAULT_VCL "/etc/varnish/default.vcl"
ENV LOG_STREAM "/tmp/stdout"

# PERMISSIONS: FILES and FOLDERS
RUN D="$PATH_VAR_VARNISH"  && mkdir -p "$D" && chgrp -R root "$D" && chmod g=u -R "$D"

RUN apk add --no-cache dcron

ADD ./docker/phpfpm/run.sh /etc/service/php-fpm/run
ADD ./docker/apache/run.sh /etc/service/apache/run
ADD ./docker/varnish/run-varnish.sh /etc/service/varnish/run
ADD ./docker/cron/run.sh /etc/service/cron/run
ADD ./docker/run.sh /run.sh
RUN chmod +x /run.sh /etc/service/*/run

ADD ./docker/varnish/magento.vcl.template /etc/varnish/magento.vcl.template
ADD ./docker/apache/conf/vhosts /opt/bitnami/apache/conf/vhosts

ADD ./docker/apache/conf/vhosts /opt/bitnami/apache/conf/vhosts

ADD ./docker/cron/crontab /etc/cron.d/magento-cron
RUN chmod 0644 /etc/cron.d/magento-cron
RUN crontab /etc/cron.d/magento-cron
RUN rm -rf /etc/environment

CMD ["/run.sh"]

FROM magento2_phpfpm_build as magento2_phpfpm_build_dev

RUN pecl install xdebug && docker-php-ext-enable xdebug

# Update INI with extensions.
RUN echo "\
\\n\
zend_extension = xdebug.so\\n\
\\n\
xdebug.remote_enable       = 1\\n\
xdebug.remote_port         = 9001\\n\
xdebug.remote_host         = host.docker.internal\\n\
xdebug.remote_connect_back = 0\\n\
xdebug.scream              = 0\\n\
xdebug.cli_color           = 1\\n\
xdebug.show_local_vars     = 1\\n\
xdebug.remote_autostart    = 1\\n\
\\n\
xdebug.var_display_max_depth    = 5\\n\
xdebug.var_display_max_children = 256\\n\
xdebug.var_display_max_data     = 1024\\n\
xdebug.max_nesting_level        = 400\\n\
\\n" >> /usr/local/etc/php/conf.d/xdebug.ini && \
  echo "opcache.enable = 0" >> /usr/local/etc/php/conf.d/opcache.ini

FROM magento2 as magento2_dev

COPY --from=magento2_phpfpm_build /usr/local/include/php/ext/* /usr/local/include/php/ext/
COPY --from=magento2_phpfpm_build /usr/local/etc/php/conf.d/* /usr/local/etc/php/conf.d/
COPY --from=magento2_phpfpm_build /usr/local/etc/php/php-fpm.d/* /usr/local/etc/php/php-fpm.d/
