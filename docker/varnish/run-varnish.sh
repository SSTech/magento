#!/bin/bash

rm -rf /etc/varnish/default.vcl
envsubst < /etc/varnish/magento.vcl.template > /etc/varnish/default.vcl

# Print commands
set -x

# Start varnish
varnishd -f "$FILE_DEFAULT_VCL" -s "malloc,$VARNISH_MEM"

varnishncsa -F"%{X-Forwarded-For}i %l %u %t \"%r\" %s %b \"%{Referer}i\" \"%{User-agent}i\" %D %{Varnish:hitmiss}x"
